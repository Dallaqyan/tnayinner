import os
class Bike:
    """This class to work bike"""

    def __init__(self, color, wheels_color, light):
        self.wheels_color = wheels_color
        self.color = color
        self.pos_x = 0
        self.pos_y = 0
        self.number_pf_wheels = 2
        self.light = light

    def to_drive(self, forward = 0, left = 0, right = 0):
        self.pos_x = self.pos_y + forward
        self.pos_y = self.pos_y + right - left

    def talk(self, text = ''):
        print(f"I say: {text}")

d_kall = Bike(color='GREEN', wheels_color='blue', light='on')

d_kall.to_drive(forward=2)

print(d_kall.pos_x)